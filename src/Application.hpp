#pragma once


#include <SFML/Graphics.hpp>
#include <chrono>

#include "instance/Instance.hpp"
#include "Layout.hpp"
#include "Splits.hpp"
#include "state/Timer.hpp"
#include "Window.hpp"
#include "Palette.hpp"
#include "state/State.hpp"



class Application {

public:
    std::filesystem::path instancePath;
    Instance instance;

    std::filesystem::path statePath;
    State state;

    // Layout filename is in `instance`
    Layout layout;

    // [TODO] Splits filename is in `instance`
    Splits splits;



    std::optional<Counter> getCounter(std::string id);
    std::optional<Timer> getTimer(std::string id);
    std::optional<std::string> getVariable(std::string name);

    void start(std::vector<std::string>& args);
    void stop();
    bool isRunning();

    // [TODO]
    // Save state
    // Reload instance, layout and splits data from files
    // Refresh layout, reopen all of the windows
    void reload();

    static void init(std::vector<std::string>& args);
    static Application& inst();

private:
    // Load a file (state or instance)
    void loadFile(std::filesystem::path filename);

    // Load instance from file or JSON
    void loadInstance(std::filesystem::path filename);
    void loadInstance(std::shared_ptr<tu::json::Node> json);

    // Load the state from file
    void loadState(std::filesystem::path filename);
    void loadState(std::shared_ptr<tu::json::Node> json);

    // Load the layout from file
    // Filename is stored inside of `instance`
    void loadLayout();

    // Load the splits from file
    // Filename is stored inside of `instance`
    void loadSplits();


    // Save current state to file
    void saveState();
    void saveState(std::filesystem::path filename);

    
    void loop(float deltaTime, std::chrono::system_clock::time_point currentTime);
    void handleKeyEvent(sf::Event::KeyEvent e);
    void checkGlobalKeys();
    void close();

    std::vector<bool> lastKeyboardState;
    std::vector<bool> currentKeyboardState;

    bool running;

    static Application* __instance;

};
