#include "Palette.hpp"



sf::Color Palette::get(std::string colorName) {
    if (colorName.empty()) return sf::Color::Transparent;

    if (colorName[0] == '#') {
        try {
            std::int64_t value = std::stoll(colorName.substr(1), 0, 16);
            // If the color is RGB and not RGBA, set alpha to 0xff (100%)
            if (colorName.length() <= 7) {
                value <<= 8;
                value |= 0xff;
            }
            return sf::Color(value);
        } catch (std::exception& e) {
        }
    }

    auto it = this->colors.find(colorName);
    if (it != this->colors.end()) {
        return it->second;
    }
    return sf::Color::Transparent;
}

void Palette::set(std::string colorName, sf::Color color) {
    this->colors[colorName] = color;
}
