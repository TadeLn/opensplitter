#pragma once


#include "Component.hpp"
#include "style/TextStyle.hpp"
#include "style/ComponentStyle.hpp"
#include "textElement/Element.hpp"



class SplitsComponent : public Component {

public:
    ComponentStyle componentStyle;
    // SplitsStyle splitsStyle;

    virtual void draw(std::shared_ptr<sf::RenderTarget> win, sf::Vector2i& offset, Direction direction, int size);

    SplitsComponent();

    static std::unique_ptr<SplitsComponent> fromJSON(std::shared_ptr<tu::json::Node> json);

};
