#pragma once


#include "Component.hpp"



class InvalidComponent : public Component {

public:
    virtual void draw(std::shared_ptr<sf::RenderTarget> win, sf::Vector2i& offset, Direction direction, int size);

    static std::unique_ptr<InvalidComponent> fromJSON(std::shared_ptr<tu::json::Node> json);

};
