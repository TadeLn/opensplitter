#include "Component.hpp"


#include <iostream>

#include "util/Util.hpp"
#include "TextComponent.hpp"
#include "SplitsComponent.hpp"
#include "InvalidComponent.hpp"



void Component::draw(std::shared_ptr<sf::RenderTarget> win, sf::Vector2i& offset, Direction direction, int size) {
}

void Component::moveOffset(sf::Vector2i& offset, Direction direction, int amount) {
    if (direction == Direction::HORIZONTAL) {
        offset.x += amount;
    } else if (direction == Direction::VERTICAL) {
        offset.y += amount;
    }
}



std::unique_ptr<Component> Component::fromJSON(std::shared_ptr<tu::json::Node> json) {
    std::string componentType = json->getProperty("type")->getOr<std::string>();

    if (componentType == "text")   return TextComponent::fromJSON(json);
    if (componentType == "splits") return SplitsComponent::fromJSON(json);

    // If all above fails:
    return InvalidComponent::fromJSON(json);
}
