#include "TextComponent.hpp"


#include <iostream>

#include "util/Util.hpp"
#include "font/FontManager.hpp"



void TextComponent::draw(std::shared_ptr<sf::RenderTarget> win, sf::Vector2i& offset, Direction direction, int size) {
    std::vector<std::string> args;
    for (auto elem : this->elements) {
        args.push_back(elem->getValue());
    }
    std::string formattedString = Util::formatString(this->text, args);


    sf::RectangleShape rect;
    this->componentStyle.apply(rect, direction, size);
    rect.move(offset.x, offset.y);
    win->draw(rect);

    sf::Text text;
    this->textStyle.apply(text);
    text.move(offset.x, offset.y);
    text.setString(formattedString);
    win->draw(text);

    moveOffset(offset, direction, this->componentStyle.size);
}



TextComponent::TextComponent() {
    this->text = "Example text";
    this->componentStyle = ComponentStyle();
    this->textStyle = TextStyle();
}



std::unique_ptr<TextComponent> TextComponent::fromJSON(std::shared_ptr<tu::json::Node> json) {
    std::unique_ptr<TextComponent> result = std::make_unique<TextComponent>();

    json->getProperty("text")->load(result->text);
    auto elements = json->getProperty("elements")->getOr<tu::json::Node::array>();
    for (auto element : elements) {
        result->elements.push_back(Element::fromJSON(element));
    }
    result->componentStyle = ComponentStyle::fromJSON(json->getProperty("componentStyle"));
    result->textStyle = TextStyle::fromJSON(json->getProperty("textStyle"));

    return result;
}