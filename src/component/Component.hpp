#pragma once


#include <SFML/Graphics.hpp>

#include "tutils/json/Deserializable.hpp"

#include "util/Direction.hpp"



class Component {

public:
    virtual void draw(std::shared_ptr<sf::RenderTarget> win, sf::Vector2i& offset, Direction direction, int size);

    static void moveOffset(sf::Vector2i& offset, Direction direction, int amount);

    static std::unique_ptr<Component> fromJSON(std::shared_ptr<tu::json::Node> json);

};
