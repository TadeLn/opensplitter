#include "SplitsComponent.hpp"


#include <iostream>

#include "util/Util.hpp"
#include "font/FontManager.hpp"



void SplitsComponent::draw(std::shared_ptr<sf::RenderTarget> win, sf::Vector2i& offset, Direction direction, int size) {
    sf::RectangleShape rect;
    this->componentStyle.apply(rect, direction, size);
    rect.move(offset.x, offset.y);
    win->draw(rect);

    for (std::uint32_t i = 0; i < 5; i++) {
        sf::Text text;
        TextStyle().apply(text);
        text.move(offset.x, offset.y + (i * 30));
        text.setString("Splits component #" + std::to_string(i));
        win->draw(text);
    }

    moveOffset(offset, direction, this->componentStyle.size);
}



SplitsComponent::SplitsComponent() {
    this->componentStyle = ComponentStyle();
}



std::unique_ptr<SplitsComponent> SplitsComponent::fromJSON(std::shared_ptr<tu::json::Node> json) {
    std::unique_ptr<SplitsComponent> result = std::make_unique<SplitsComponent>();

    result->componentStyle = ComponentStyle::fromJSON(json->getProperty("componentStyle"));
    // result->splitsStyle = SplitsStyle::fromJSON(json->getProperty("splitsStyle"));

    return result;
}