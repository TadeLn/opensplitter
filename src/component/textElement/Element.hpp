#pragma once


#include <string>
#include <memory>

#include "tutils/json/Node.hpp"



class Element {

public:
    virtual std::string getValue() = 0;

    static std::unique_ptr<Element> fromJSON(std::shared_ptr<tu::json::Node> json);

};
