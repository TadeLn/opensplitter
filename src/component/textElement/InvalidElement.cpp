#include "InvalidElement.hpp"



std::string InvalidElement::getValue() {
    return "<invalid>";
}

std::unique_ptr<InvalidElement> InvalidElement::fromJSON(std::shared_ptr<tu::json::Node> json) {
    return std::make_unique<InvalidElement>();
}
