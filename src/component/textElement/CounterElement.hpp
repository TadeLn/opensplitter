#pragma once


#include <string>
#include <memory>

#include "tutils/json/Node.hpp"

#include "Element.hpp"



class CounterElement : public Element {

public:
    std::string id;

    std::string getValue();

    static std::unique_ptr<CounterElement> fromJSON(std::shared_ptr<tu::json::Node> json);

};
