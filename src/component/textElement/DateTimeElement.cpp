#include "DateTimeElement.hpp"


#include <sstream>
#include <iomanip>
#include <ctime>

#include "Application.hpp"



std::string DateTimeElement::getValue() {
    std::time_t t = std::time(nullptr);
    const char* formatString;
    std::stringstream ss;

    switch (fragment) {
        case YEAR:   formatString = "%Y"; break;
        case MONTH:  formatString = "%m"; break;
        case DAY:    formatString = "%d"; break;
        case HOUR:   formatString = this->_12hours ? "%I" : "%H"; break;
        case MINUTE: formatString = "%M"; break;
        case SECOND: formatString = "%S"; break;
        default: return "<invalid fragment>";
    }

    ss << std::put_time(std::localtime(&t), formatString);
    return ss.str();
}

std::unique_ptr<DateTimeElement> DateTimeElement::fromJSON(std::shared_ptr<tu::json::Node> json) {
    auto result = std::make_unique<DateTimeElement>();

    std::string f = json->getProperty("fragment")->getOr<std::string>();
    if      (f == "year")   result->fragment = Fragment::YEAR;
    else if (f == "month")  result->fragment = Fragment::MONTH;
    else if (f == "day")    result->fragment = Fragment::DAY;
    else if (f == "hour")   result->fragment = Fragment::HOUR;
    else if (f == "minute") result->fragment = Fragment::MINUTE;
    else if (f == "second") result->fragment = Fragment::SECOND;
    
    json->getProperty("12hours")  ->load(result->_12hours);
    
    return result;
}
