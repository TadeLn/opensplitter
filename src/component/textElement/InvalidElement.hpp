#pragma once


#include <string>
#include <memory>

#include "tutils/json/Node.hpp"

#include "Element.hpp"



class InvalidElement : public Element {

public:
    std::string getValue();

    static std::unique_ptr<InvalidElement> fromJSON(std::shared_ptr<tu::json::Node> json);

};
