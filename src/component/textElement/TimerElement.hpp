#pragma once


#include <string>
#include <memory>

#include "tutils/json/Node.hpp"

#include "Element.hpp"



class TimerElement : public Element {

public:
    std::string id;

    std::string getValue();

    static std::unique_ptr<TimerElement> fromJSON(std::shared_ptr<tu::json::Node> json);

};
