#include "CounterElement.hpp"


#include "Application.hpp"



std::string CounterElement::getValue() {
    std::optional<Counter> counter = Application::inst().getCounter(this->id);
    if (counter.has_value()) {
        return std::to_string(counter.value().getValue());
    } else {
        return "<undefined counter: " + this->id + ">";
    }
}

std::unique_ptr<CounterElement> CounterElement::fromJSON(std::shared_ptr<tu::json::Node> json) {
    auto result = std::make_unique<CounterElement>();
    json->getProperty("id")->load(result->id);
    return result;
}
