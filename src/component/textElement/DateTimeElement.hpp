#pragma once


#include <string>
#include <memory>

#include "tutils/json/Node.hpp"

#include "Element.hpp"



class DateTimeElement : public Element {

public:
    enum Fragment {
        INVALID,
        YEAR,
        MONTH,
        DAY,
        HOUR,
        MINUTE,
        SECOND
    };
    Fragment fragment = INVALID;
    bool _12hours = false;

    std::string getValue();

    static std::unique_ptr<DateTimeElement> fromJSON(std::shared_ptr<tu::json::Node> json);

};
