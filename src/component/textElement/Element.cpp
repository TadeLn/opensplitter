#include "Element.hpp"


#include "VariableElement.hpp"
#include "CounterElement.hpp"
#include "TimerElement.hpp"
#include "DateTimeElement.hpp"
#include "InvalidElement.hpp"



std::unique_ptr<Element> Element::fromJSON(std::shared_ptr<tu::json::Node> json) {
    std::string type = json->getProperty("type")->getOr<std::string>();

    if (type == "variable") return VariableElement::fromJSON(json);
    if (type == "counter")  return CounterElement::fromJSON(json);
    if (type == "timer")    return TimerElement::fromJSON(json);
    if (type == "datetime") return DateTimeElement::fromJSON(json);

    // If all above fails:
    return InvalidElement::fromJSON(json);
}
