#include "VariableElement.hpp"


#include "Application.hpp"

#include "util/Util.hpp"



std::string VariableElement::getValue() {
    auto variable = Application::inst().getVariable(this->name);
    if (variable.has_value()) {
        return variable.value();
    } else {
        return "<undefined variable: " + this->name + ">";
    }
}

std::unique_ptr<VariableElement> VariableElement::fromJSON(std::shared_ptr<tu::json::Node> json) {
    auto result = std::make_unique<VariableElement>();
    json->getProperty("name")->load(result->name);
    return result;
}
