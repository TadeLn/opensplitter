#include "TimerElement.hpp"


#include "Application.hpp"

#include "util/Util.hpp"



std::string TimerElement::getValue() {
    auto timer = Application::inst().getTimer(this->id);
    if (timer.has_value()) {
        return Util::msToString(timer.value().get(), 3);
    } else {
        return "<undefined timer: " + this->id + ">";
    }
}

std::unique_ptr<TimerElement> TimerElement::fromJSON(std::shared_ptr<tu::json::Node> json) {
    auto result = std::make_unique<TimerElement>();
    json->getProperty("id")->load(result->id);
    return result;
}
