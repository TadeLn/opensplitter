#pragma once


#include <string>
#include <memory>

#include "tutils/json/Node.hpp"

#include "Element.hpp"



class VariableElement : public Element {

public:
    std::string name;

    std::string getValue();

    static std::unique_ptr<VariableElement> fromJSON(std::shared_ptr<tu::json::Node> json);

};
