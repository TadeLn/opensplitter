#include "InvalidComponent.hpp"


#include <iostream>

#include "font/FontManager.hpp"



void InvalidComponent::draw(std::shared_ptr<sf::RenderTarget> win, sf::Vector2i& offset, Direction direction, int size) {
    const int rectThickness = 30;

    sf::RectangleShape rect;
    rect.setFillColor(sf::Color::Red);
    rect.setPosition(offset.x, offset.y);

    if (direction == Direction::HORIZONTAL) {
        rect.setSize(sf::Vector2f(rectThickness, size));
    } else {
        rect.setSize(sf::Vector2f(size, rectThickness));
    }

    win->draw(rect);


    sf::Text text;
    text.setFont(*FontManager::getDefault());
    text.setCharacterSize(rectThickness);
    text.setString("Invalid Component");
    text.setPosition(offset.x, offset.y);
    if (direction == Direction::HORIZONTAL) {
        text.setRotation(90);
    }
    text.setFillColor(sf::Color::White);
    win->draw(text);
    

    moveOffset(offset, direction, rectThickness);
}

std::unique_ptr<InvalidComponent> InvalidComponent::fromJSON(std::shared_ptr<tu::json::Node> json) {
    return std::make_unique<InvalidComponent>();
}
