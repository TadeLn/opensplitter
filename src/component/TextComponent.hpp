#pragma once


#include "Component.hpp"
#include "style/TextStyle.hpp"
#include "style/ComponentStyle.hpp"
#include "textElement/Element.hpp"



class TextComponent : public Component {

public:
    std::string text;
    std::vector<std::shared_ptr<Element>> elements;
    ComponentStyle componentStyle;
    TextStyle textStyle;

    virtual void draw(std::shared_ptr<sf::RenderTarget> win, sf::Vector2i& offset, Direction direction, int size);

    TextComponent();

    static std::unique_ptr<TextComponent> fromJSON(std::shared_ptr<tu::json::Node> json);

};
