#include "Layout.hpp"


#include <iostream>
#include <fstream>

#include "tutils/json/Object.hpp"

#include "font/FontManager.hpp"
#include "util/Util.hpp"
#include "Application.hpp"



std::shared_ptr<Window> Layout::getWindow(std::string windowId) {
    return this->windows.at(windowId);
}

std::vector<std::string> Layout::getWindowIds() {
    std::vector<std::string> key;
    for (std::pair<std::string, std::shared_ptr<Window>> it : windows) {
        key.push_back(it.first);
    }
    return key;
}

const std::map<std::string, std::shared_ptr<Window>> Layout::getWindows() {
    return this->windows;
}



Layout::Layout() {
    this->windows = std::map<std::string, std::shared_ptr<Window>>();
}



void Layout::loadJSONEx(std::shared_ptr<tu::json::Node> json) {
    using namespace tu::json;
    
    auto palette = json->getProperty("palette")->getOr<Node::dictionary>();
    for (auto it : palette) {
        auto color = Util::getColor(it.second);
        if (color.has_value()) {
            this->palette.set(it.first, color.value());
        }
    }

    State& state = Application::inst().state;

    auto counters = json->getProperty("counters")->getOr<Node::dictionary>();
    for (auto pair : counters) {
        auto counter = Counter::fromJSON(pair.second);
        auto it = state.counters.find(pair.first);
        if (it != state.counters.end()) {
            counter.copyStateFrom(it->second);
        }
        state.counters[pair.first] = counter;
    }

    auto timers = json->getProperty("timers")->getOr<Node::dictionary>();
    for (auto pair : timers) {
        auto timer = Timer::fromJSON(pair.second);
        auto it = state.timers.find(pair.first);
        if (it != state.timers.end()) {
            timer.copyStateFrom(it->second);
            timer.get(std::chrono::system_clock::now(), true);
        }
        state.timers[pair.first] = timer;
    }

    auto windows = json->getProperty("windows")->getOr<Node::dictionary>();
    for (auto it : windows) {
        auto window = Window::fromJSON(it.second);
        this->windows[it.first] = window;
    }
}

Layout Layout::fromJSON(std::shared_ptr<tu::json::Node> json) {
    Layout result;
    result.loadJSONEx(json);
    return result;
}

Layout Layout::fromFile(std::string filename) {
    return fromJSON(tu::json::Node::fromFileEx(filename));
}
