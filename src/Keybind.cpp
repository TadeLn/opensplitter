#include "Keybind.hpp"


#include "util/Util.hpp"



bool Keybind::checkEvent(sf::Event::KeyEvent e) {
    if (code == -1) return false;

    if (code != e.code) {
        return false;
    }
    if (control.has_value() && e.control != control.value()) {
        return false;
    }
    if (alt.has_value() && e.alt != alt.value()) {
        return false;
    }
    if (shift.has_value() && e.shift != shift.value()) {
        return false;
    }
    if (system.has_value() && e.system != system.value()) {
        return false;
    }
    return true;
}



Keybind::Keybind(int code, std::optional<bool> control, std::optional<bool> alt, std::optional<bool> shift, std::optional<bool> system) {
    this->code = code;
    this->control = control;
    this->alt = alt;
    this->shift = shift;
    this->system = system;
}

Keybind::Keybind()
: Keybind((int)-1, std::optional<bool>(), std::optional<bool>(), std::optional<bool>(), std::optional<bool>()) {}



void Keybind::loadJSONEx(std::shared_ptr<tu::json::Node> json) {
    json->getProperty("code")->load(code);
    control = json->getProperty("control")->get<bool>();
    shift   = json->getProperty("shift")  ->get<bool>();
    alt     = json->getProperty("alt")    ->get<bool>();
    system  = json->getProperty("system") ->get<bool>();
}
   
Keybind Keybind::fromJSON(std::shared_ptr<tu::json::Node> json) {
    Keybind result;
    result.loadJSONEx(json);
    return result;
}
