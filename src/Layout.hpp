#pragma once


#include <SFML/Graphics.hpp>

#include <string>
#include <map>
#include <vector>

#include "tutils/json/Deserializable.hpp"

#include "Window.hpp"
#include "Palette.hpp"
#include "state/Counter.hpp"



class Layout : public tu::json::Deserializable {


public:
    // sf::Font* font;
    // sf::Color backgroundColor = sf::Color(0x101010ff);
    // sf::Color fontColor = sf::Color(0xffffffff);

    // sf::Color timeSave;
    // sf::Color timeLoss;
    // sf::Color bestEverSplit;

    // int decimalPlaces = 3;
    
    Palette palette;

    std::shared_ptr<Window> getWindow(std::string windowId);
    std::vector<std::string> getWindowIds();
    const std::map<std::string, std::shared_ptr<Window>> getWindows();

    Layout();

    void loadJSONEx(std::shared_ptr<tu::json::Node> json);
    static Layout fromJSON(std::shared_ptr<tu::json::Node> json);
    static Layout fromFile(std::string filename);

private:
    std::map<std::string, std::shared_ptr<Window>> windows;


};
