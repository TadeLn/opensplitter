#pragma once


#include <string>
#include <vector>

#include "Splits.hpp"


class Run {

public:
    std::string name;
    std::string player;
    std::vector<unsigned int> splitTimes;

    unsigned int runNo;

    Splits splits;

};
