#include "Splits.hpp"


#include <iostream>
#include "util/Util.hpp"



Splits::Splits() {
    this->game = "<game unspecified>";
    this->category = "<category unspecified>";
}



void Splits::loadJSONEx(std::shared_ptr<tu::json::Node> json) {
    using namespace tu::json;

    json->getProperty("game")->load(game);
    json->getProperty("category")->load(category);

    auto splits = json->getProperty("splits")->getOr<Node::array>();
    this->splits.clear();
    for (auto elem : splits) {
        this->splits.push_back(Split::fromJSON(elem));
    }
}

Splits Splits::fromJSON(std::shared_ptr<tu::json::Node> json) {
    Splits result;
    result.loadJSONEx(json);
    return result;
}

Splits Splits::fromFile(std::string filename) {
    return fromJSON(tu::json::Node::fromFileEx(filename));
}
