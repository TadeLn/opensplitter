#include "Counter.hpp"


#include <iostream>

#include "tutils/json/Int.hpp"



std::int64_t Counter::getValue() {
    return value;
}

void Counter::setValue(std::int64_t value) {
    this->value = value;
}

std::int64_t Counter::getMin() {
    return min;
}

std::int64_t Counter::getMax() {
    return max;
}



void Counter::handleKeyEvent(sf::Event::KeyEvent e) {
    if (keybinds.increment.checkEvent(e)) this->value++;
    if (keybinds.decrement.checkEvent(e)) this->value--;
    
    if (this->value < this->min) this->value = this->min;
    if (this->value > this->max) this->value = this->max;
}



void Counter::loadJSONEx(std::shared_ptr<tu::json::Node> json) {
    json->getProperty("defaultValue")->load(value);
    json->getProperty("min")         ->load(min);
    json->getProperty("max")         ->load(max);
    
    auto k = json->getProperty("keybinds");
    keybinds.increment = Keybind::fromJSON(k->getProperty("increment"));
    keybinds.decrement = Keybind::fromJSON(k->getProperty("decrement"));
}

Counter Counter::fromJSON(std::shared_ptr<tu::json::Node> json) {
    Counter result;
    result.loadJSONEx(json);
    return result;
}



void Counter::loadState(std::shared_ptr<tu::json::Node> json) {
    value = json->getOr<std::int64_t>();
}

std::shared_ptr<tu::json::Node> Counter::saveState() {
    return std::make_shared<tu::json::Int>(value);
}

void Counter::copyStateFrom(Counter& c) {
    value = c.value;
}
