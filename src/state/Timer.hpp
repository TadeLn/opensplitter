#pragma once


#include <chrono>

#include "tutils/json/Deserializable.hpp"

#include "Keybind.hpp"


class Timer : public tu::json::Deserializable {

public:
    void start (std::chrono::system_clock::time_point timestamp = std::chrono::system_clock::now());
    void stop  (std::chrono::system_clock::time_point timestamp = std::chrono::system_clock::now());
    void pause (std::chrono::system_clock::time_point timestamp = std::chrono::system_clock::now());
    void resume(std::chrono::system_clock::time_point timestamp = std::chrono::system_clock::now());
    void reset ();

    std::int64_t get(std::chrono::system_clock::time_point timestamp = std::chrono::system_clock::now(), bool forceRefresh = false);

    bool isRunning();
    bool isPaused();

    void handleKeyEvent(sf::Event::KeyEvent e);

    void loadJSONEx(std::shared_ptr<tu::json::Node> json);
    static Timer fromJSON(std::shared_ptr<tu::json::Node> json);

    void loadState(std::shared_ptr<tu::json::Node> json);
    std::shared_ptr<tu::json::Node> saveState();
    void copyStateFrom(Timer& c);

private:
    struct {
        Keybind start;
        Keybind stop;
        Keybind toggleStartStop;
        Keybind reset;

        Keybind pause;
        Keybind resume;
        Keybind togglePause;
    } keybinds;

    // Is the timer currently running (not stopped and not paused)
    bool running = false;

    // Timestamp of the beginning of counting time
    std::chrono::system_clock::time_point startTimestamp;

    // Offset of the beginning
    std::int64_t startOffset = 0;

    // Latest calculated time in ms
    std::int64_t latestTime = 0;

    // Is the timer currently paused
    bool paused = false;

    // The total amount of time spent paused since startTimestamp
    std::chrono::system_clock::duration pauseTime;

    // Timestamp of the beginning of the current pause
    // Not important when not paused
    std::chrono::system_clock::time_point pauseTimestamp;

    // Timestamp of the end of counting time
    std::chrono::system_clock::time_point stopTimestamp;

};