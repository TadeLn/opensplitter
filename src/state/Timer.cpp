#include "Timer.hpp"


#include <iostream>

#include "tutils/Log.hpp"
#include "tutils/json/Object.hpp"
#include "tutils/json/Int.hpp"
#include "tutils/json/Bool.hpp"



void Timer::start(std::chrono::system_clock::time_point timestamp) {
    pauseTime = std::chrono::system_clock::duration();
    startTimestamp = timestamp;
    stopTimestamp = timestamp;
    running = true;
    paused = false;
}

void Timer::stop(std::chrono::system_clock::time_point timestamp) {
    if (!running && !paused) {
        tu::Log::warn("Cannot stop when not running!");
        return;
    }
    get(timestamp);
    stopTimestamp = timestamp;
    running = false;
}

void Timer::pause(std::chrono::system_clock::time_point timestamp) {
    if (!running || paused) {
        tu::Log::warn("Cannot pause when not running!");
        return;
    }
    get(timestamp);
    pauseTimestamp = timestamp;
    running = false;
    paused = true;
}

void Timer::resume(std::chrono::system_clock::time_point timestamp) {
    if (running || !paused) {
        tu::Log::warn("Cannot resume when not paused!");
        return;
    }
    pauseTime += timestamp - pauseTimestamp;
    running = true;
    paused = false;
}



void Timer::reset() {
    latestTime = 0;
    pauseTime = std::chrono::milliseconds(0);
    running = false;
    paused = false;
    startTimestamp = std::chrono::system_clock::time_point();
    stopTimestamp = std::chrono::system_clock::time_point();
}


std::int64_t Timer::get(std::chrono::system_clock::time_point timestamp, bool forceRefresh) {
    using namespace std::chrono;
    if (running || forceRefresh) {
        if (!running && !paused) {
            // The timer hasn't started yet, or has finished already (before start or after stop)
            if (startTimestamp == stopTimestamp) {
                // The timer hasn't started yet
                latestTime = 0;
            } else {
                latestTime = duration_cast<milliseconds>(
                    // Calculate the difference between `stopTimestamp` and startTimestamp`
                    // Also, subtract the total `pauseTime`
                    (stopTimestamp - startTimestamp) - pauseTime
                ).count();
            }

        } else {
            latestTime = duration_cast<milliseconds>(
                // If the timer is paused, add the `-currentPauseLength` (== `pauseTimestamp - timestamp`), which effectively cancels out the current pause (which is not included in the `pauseTime`)
                // If the timer is not paused, don't add anything
                (paused ? (pauseTimestamp - timestamp) : milliseconds(0)) +

                // Calculate the difference between `timestamp` and startTimestamp`
                // Also, subtract the total `pauseTime`
                (timestamp - startTimestamp) - pauseTime
            ).count();
        }
    }
    return latestTime;
}



bool Timer::isRunning() {
    return running;
}

bool Timer::isPaused() {
    return paused;
}



void Timer::handleKeyEvent(sf::Event::KeyEvent e) {
    if (keybinds.start.checkEvent(e)) start();
    if (keybinds.stop.checkEvent(e))  stop();
    if (keybinds.reset.checkEvent(e)) reset();
    if (keybinds.toggleStartStop.checkEvent(e)) {
        if (!isRunning()) start();
        else stop();
    }

    if (keybinds.pause.checkEvent(e))  pause();
    if (keybinds.resume.checkEvent(e)) resume();
    if (keybinds.togglePause.checkEvent(e)) {
        if (!isPaused()) pause();
        else resume();
    }
}



void Timer::loadJSONEx(std::shared_ptr<tu::json::Node> json) {
    json->getProperty("startOffset")->load(startOffset);
    
    auto k = json->getProperty("keybinds");
    keybinds.start           = Keybind::fromJSON(k->getProperty("start"));
    keybinds.stop            = Keybind::fromJSON(k->getProperty("stop"));
    keybinds.toggleStartStop = Keybind::fromJSON(k->getProperty("toggleStartStop"));
    keybinds.reset           = Keybind::fromJSON(k->getProperty("reset"));
    keybinds.pause           = Keybind::fromJSON(k->getProperty("pause"));
    keybinds.resume          = Keybind::fromJSON(k->getProperty("resume"));
    keybinds.togglePause     = Keybind::fromJSON(k->getProperty("togglePause"));
}

Timer Timer::fromJSON(std::shared_ptr<tu::json::Node> json) {
    Timer result;
    result.loadJSONEx(json);
    return result;
}



void Timer::loadState(std::shared_ptr<tu::json::Node> json) {
    using namespace tu::json;
    using namespace std::chrono;
    startOffset    = json->getProperty("startOffset")->getOr<std::int64_t>();
    startTimestamp = system_clock::time_point(milliseconds(json->getProperty("startTimestamp")->getOr<std::int64_t>()));
    running        = json->getProperty("running")->getOr<bool>();
    paused         = json->getProperty("paused")->getOr<bool>();
    pauseTime      = milliseconds(json->getProperty("pauseTime")->getOr<std::int64_t>());
    pauseTimestamp = system_clock::time_point(milliseconds(json->getProperty("pauseTimestamp")->getOr<std::int64_t>()));
    stopTimestamp  = system_clock::time_point(milliseconds(json->getProperty("stopTimestamp")->getOr<std::int64_t>()));
}

std::shared_ptr<tu::json::Node> Timer::saveState() {
    using namespace tu::json;
    using namespace std::chrono;
    auto json = std::make_shared<Object>();
    json->setProperty("startOffset",    std::make_shared<Int>(startOffset));
    json->setProperty("startTimestamp", std::make_shared<Int>(duration_cast<milliseconds>(startTimestamp.time_since_epoch()).count()));
    json->setProperty("running",        std::make_shared<Bool>(running));
    json->setProperty("paused",         std::make_shared<Bool>(paused));
    json->setProperty("pauseTime",      std::make_shared<Int>(duration_cast<milliseconds>(pauseTime).count()));
    json->setProperty("pauseTimestamp", std::make_shared<Int>(duration_cast<milliseconds>(pauseTimestamp.time_since_epoch()).count()));
    json->setProperty("stopTimestamp",  std::make_shared<Int>(duration_cast<milliseconds>(stopTimestamp.time_since_epoch()).count()));
    return json;
}

void Timer::copyStateFrom(Timer& c) {
    startOffset    = c.startOffset;
    startTimestamp = c.startTimestamp;
    running        = c.running;
    paused         = c.paused;
    pauseTime      = c.pauseTime;
    pauseTimestamp = c.pauseTimestamp;
    stopTimestamp  = c.stopTimestamp;
}
