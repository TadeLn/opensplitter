#pragma once


#include <map>
#include <string>
#include <filesystem>

#include "Timer.hpp"
#include "Counter.hpp"

#include "tutils/json/Deserializable.hpp"
#include "tutils/json/Serializable.hpp"



class State : public tu::json::Deserializable, public tu::json::Serializable {

public:
    std::string instance;
    std::map<std::string, Timer> timers;
    std::map<std::string, Counter> counters;

    void loadJSONEx(std::shared_ptr<tu::json::Node> json);
    std::shared_ptr<tu::json::Node> toJSON() const;
    static State fromJSON(std::shared_ptr<tu::json::Node> json);

};
