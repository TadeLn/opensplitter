#pragma once


#include "tutils/json/Deserializable.hpp"

#include "Keybind.hpp"



class Counter : public tu::json::Deserializable {

public:
    std::int64_t getValue();
    void setValue(std::int64_t value);
    std::int64_t getMin();
    std::int64_t getMax();

    void handleKeyEvent(sf::Event::KeyEvent e);

    void loadJSONEx(std::shared_ptr<tu::json::Node> json);
    static Counter fromJSON(std::shared_ptr<tu::json::Node> json);

    void loadState(std::shared_ptr<tu::json::Node> json);
    std::shared_ptr<tu::json::Node> saveState();
    void copyStateFrom(Counter& c);

private:
    struct {
        Keybind increment;
        Keybind decrement;
    } keybinds;

    std::int64_t value = 0;
    std::int64_t min = -0x8000000000000000;
    std::int64_t max = 0x7fffffffffffffff;

};
