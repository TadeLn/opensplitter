#include "State.hpp"

#include "tutils/json/Object.hpp"
#include "tutils/json/String.hpp"

#include "Application.hpp"

#include <iostream>



void State::loadJSONEx(std::shared_ptr<tu::json::Node> json) {
    using namespace tu::json;

    if (json->getProperty("type")->getOr<std::string>() != "state") throw tu::Exception("Incorrect format", EXCTX);

    json->getProperty("instance")->load(instance);

    auto timers = json->getProperty("timers")->getOr<Node::dictionary>();
    this->timers.clear();
    for (auto pair : timers) {
        Timer t;
        t.loadState(pair.second);
        this->timers[pair.first] = t;
    }

    auto counters = json->getProperty("counters")->getOr<Node::dictionary>();
    this->counters.clear();
    for (auto pair : counters) {
        Counter c;
        c.loadState(pair.second);
        this->counters[pair.first] = c;
    }
}

std::shared_ptr<tu::json::Node> State::toJSON() const {
    using namespace tu::json;
    using namespace std::chrono;

    auto json = std::make_shared<Object>();
    json->setProperty("type", std::make_shared<String>("state"));

    json->setProperty("instance", std::make_shared<String>(instance));

    auto timers = std::make_shared<Object>();
    for (auto pair : this->timers) {
        timers->setProperty(pair.first, pair.second.saveState());
    }
    json->setProperty("timers", timers);

    auto counters = std::make_shared<Object>();
    for (auto pair : this->counters) {
        counters->setProperty(pair.first, pair.second.saveState());
    }
    json->setProperty("counters", counters);

    return json;
}

State State::fromJSON(std::shared_ptr<tu::json::Node> json) {
    State state;
    state.loadJSONEx(json);
    return state;
}

