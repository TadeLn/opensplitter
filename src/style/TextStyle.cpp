#include "TextStyle.hpp"


#include <iostream>

#include "font/FontManager.hpp"
#include "util/Util.hpp"
#include "Application.hpp"



unsigned int TextStyle::getStyle() {
    unsigned int style = sf::Text::Regular;
    if (bold)          style |= sf::Text::Bold;
    if (italic)        style |= sf::Text::Italic;
    if (underlined)    style |= sf::Text::Underlined;
    if (strikethrough) style |= sf::Text::StrikeThrough;
    return style;
}

void TextStyle::apply(sf::Text& text) {
    Palette& p = Application::inst().layout.palette;

    text.setFont            (*font);
    text.setCharacterSize   (fontSize);
    text.setFillColor       (p.get(fillColor));
    text.setOutlineColor    (p.get(outlineColor));
    text.setOutlineThickness(outlineThickness);
    text.setOrigin          (origin);
    text.setPosition        (position);
    text.setRotation        (rotation);
    text.setScale           (scale);
    text.setStyle           (getStyle());
    text.setLetterSpacing   (letterSpacing);
    text.setLineSpacing     (lineSpacing);
}

TextStyle::TextStyle() {
    font = FontManager::getDefault();
    fontSize = 30;
    fillColor = "#ffffff";
    outlineColor = "#000000";
    outlineThickness = 0;
    bold = false;
    italic = false;
    underlined = false;
    strikethrough = false;
    origin = sf::Vector2f(0, 0);
    position = sf::Vector2f(0, 0);
    rotation = 0;
    scale = sf::Vector2f(1, 1);
    letterSpacing = 1;
    lineSpacing = 1;
}

void TextStyle::loadJSONEx(std::shared_ptr<tu::json::Node> json) {
    font = FontManager::get(
        json->getProperty("font")->getProperty("face"   )->getOr<std::string>(),
        json->getProperty("font")->getProperty("variant")->getOr<std::string>()
    );
    json->getProperty("fontSize"        )->load(fontSize);
    json->getProperty("fillColor"       )->load(fillColor);
    json->getProperty("outlineColor"    )->load(outlineColor);
    json->getProperty("outlineThickness")->load(outlineThickness);
    json->getProperty("bold"            )->load(bold);
    json->getProperty("italic"          )->load(italic);
    json->getProperty("underlined"      )->load(underlined);
    json->getProperty("strikethrough"   )->load(strikethrough);
    Util::load(Util::getVector2f(json->getProperty("origin"  )), origin);
    Util::load(Util::getVector2f(json->getProperty("position")), position);
    json->getProperty("rotation"        )->load(rotation);
    Util::load(Util::getVector2f(json->getProperty("scale"   )), scale);
    json->getProperty("letterSpacing"   )->load(letterSpacing);
    json->getProperty("lineSpacing"     )->load(lineSpacing);
}

TextStyle TextStyle::fromJSON(std::shared_ptr<tu::json::Node> json) {
    TextStyle result;
    result.loadJSONEx(json);
    return result;
}
