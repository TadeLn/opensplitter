#pragma once


#include <SFML/Graphics.hpp>
#include <vector>
#include <memory>

#include "tutils/json/Node.hpp"
#include "tutils/json/Deserializable.hpp"



class TextStyle : public tu::json::Deserializable {

public:
    std::shared_ptr<sf::Font> font;
    unsigned int fontSize;
    std::string fillColor;
    std::string outlineColor;
    float outlineThickness;
    bool bold;
    bool italic;
    bool underlined;
    bool strikethrough;
    sf::Vector2f origin;
    sf::Vector2f position;
    float rotation;
    sf::Vector2f scale;
    float letterSpacing;
    float lineSpacing;

    unsigned int getStyle();
    void apply(sf::Text& text);

    TextStyle();
    
    void loadJSONEx(std::shared_ptr<tu::json::Node> json);
    static TextStyle fromJSON(std::shared_ptr<tu::json::Node> json);

};
