#pragma once


#include <vector>
#include <memory>

#include <SFML/Graphics.hpp>

#include "tutils/json/Node.hpp"
#include "tutils/json/Deserializable.hpp"

#include "util/Direction.hpp"



class ComponentStyle : public tu::json::Deserializable {

public:
    std::string fillColor;
    int size;

    void apply(sf::RectangleShape& rectangle, Direction layoutDirection, int size);

    ComponentStyle();
    
    void loadJSONEx(std::shared_ptr<tu::json::Node> json);
    static ComponentStyle fromJSON(std::shared_ptr<tu::json::Node> json);

};
