#pragma once


#include <SFML/Graphics.hpp>

#include "util/Direction.hpp"

#include "tutils/json/Deserializable.hpp"



class WindowStyle : public tu::json::Deserializable {

public:
    std::string title;
    unsigned int width;
    unsigned int height;
    bool resizable;
    std::string backgroundColor;
    Direction layoutDirection;

    WindowStyle();
    
    void loadJSONEx(std::shared_ptr<tu::json::Node> json);
    static WindowStyle fromJSON(std::shared_ptr<tu::json::Node> json);

};
