#include "ComponentStyle.hpp"


#include <iostream>

#include "util/Util.hpp"



void ComponentStyle::apply(sf::RectangleShape& rect, Direction layoutDirection, int size) {
    Palette& p = Application::inst().layout.palette;

    if (layoutDirection == Direction::HORIZONTAL) {
        rect.setSize(sf::Vector2f(this->size, size));
    } else if (layoutDirection == Direction::VERTICAL) {
        rect.setSize(sf::Vector2f(size, this->size));
    }
    rect.setFillColor(p.get(this->fillColor));
}



ComponentStyle::ComponentStyle() {
    this->fillColor = "";
    this->size = 40;
}



void ComponentStyle::loadJSONEx(std::shared_ptr<tu::json::Node> json) {
    json->getProperty("fillColor")->load(this->fillColor);
    json->getProperty("size")     ->load(this->size);
}

ComponentStyle ComponentStyle::fromJSON(std::shared_ptr<tu::json::Node> json) {
    ComponentStyle result;
    result.loadJSONEx(json);
    return result;
}