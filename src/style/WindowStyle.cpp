#include "WindowStyle.hpp"


#include "tutils/json/Object.hpp"

#include "util/Util.hpp"



WindowStyle::WindowStyle() {
    this->width = 300;
    this->height = 300;
    this->resizable = false;
    this->backgroundColor = "";
    this->layoutDirection = Direction::HORIZONTAL;
}

void WindowStyle::loadJSONEx(std::shared_ptr<tu::json::Node> json) {
    json->getProperty("title"          )->load(this->title);
    json->getProperty("width"          )->load(this->width);
    json->getProperty("height"         )->load(this->height);
    json->getProperty("resizable"      )->load(this->resizable);
    json->getProperty("backgroundColor")->load(this->backgroundColor);

    std::string str = "";
    json->getProperty("layoutDirection")->load(str);
    if (str == "horizontal") {
        this->layoutDirection = Direction::HORIZONTAL;
    } else if (str == "vertical") {
        this->layoutDirection = Direction::VERTICAL;
    }
}

WindowStyle WindowStyle::fromJSON(std::shared_ptr<tu::json::Node> json) {
    WindowStyle result;
    result.loadJSONEx(json);
    return result;
}
