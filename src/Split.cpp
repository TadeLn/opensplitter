#include "Split.hpp"


#include "util/Util.hpp"



Split::Split() {
    this->name = "Split";
    this->desc = "";
}



void Split::loadJSONEx(std::shared_ptr<tu::json::Node> json) {
    json->getProperty("name")->load(name);
    json->getProperty("desc")->load(desc);
}

Split Split::fromJSON(std::shared_ptr<tu::json::Node> json) {
    Split result;
    result.loadJSONEx(json);
    return result;
}

