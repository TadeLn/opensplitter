#pragma once


#include <SFML/Graphics.hpp>
#include <string>
#include <map>



class Palette {


public:
    sf::Color get(std::string colorName);
    void set(std::string colorName, sf::Color color);

private:
    std::map<std::string, sf::Color> colors;


};
