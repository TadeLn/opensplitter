#include "Window.hpp"


#include <iostream>
#include <vector>

#include "tutils/Log.hpp"

#include "util/Util.hpp"



WindowStyle Window::getWindowStyle() {
    return this->style;
}

void Window::setWindowStyle(WindowStyle style) {
    this->style = style;
    this->recreateWindow();
}


void Window::open() {
    this->win = std::make_shared<sf::RenderWindow>();

    unsigned int windowStyle = sf::Style::Titlebar | sf::Style::Close;
    if (this->style.resizable) windowStyle |= sf::Style::Resize;

    this->win->create(sf::VideoMode(this->style.width, this->style.height), this->style.title, windowStyle);

    std::string pointerStr = std::string("0x") + Util::toHex((std::uint64_t)this->win.get());
    // this->win->setTitle(std::string("Window ") + pointerStr);
    this->win->setVerticalSyncEnabled(true);
    this->win->setFramerateLimit(60);

    tu::Log::log("Opened window ", pointerStr);
}

void Window::close() {
    if (this->win != nullptr) {
        std::string pointerStr = std::string("0x") + Util::toHex((std::uint64_t)this->win.get());

        this->win->close();
        this->win.reset();

        tu::Log::log("Closed window ", pointerStr);
    }
}

void Window::recreateWindow() {
    if (this->win.get() != nullptr) {
        close();
        open();
    }
}


void Window::draw() {
    Palette& p = Application::inst().layout.palette;
    win->clear(p.get(style.backgroundColor));

    sf::Vector2i offset;
    int size = 0;
    if (style.layoutDirection == Direction::HORIZONTAL) {
        size = win->getSize().y;
    } else if (style.layoutDirection == Direction::VERTICAL) {
        size = win->getSize().x;
    }

    for (int i = 0; i < this->components.size(); i++) {
        this->components[i]->draw(this->win, offset, style.layoutDirection, size);
    }

    // sf::Text timerText;
    // timerText.setFont(*layout.font);
    // timerText.setFillColor(layout.fontColor);
    // timerText.setCharacterSize(50);
    // timerText.setString(Util::msToString(timer.get(currentTime), layout.decimalPlaces));
    // win->draw(timerText);

    // sf::Text splitText;
    // splitText.setFont(*layout.font);
    // splitText.setCharacterSize(20);
    // for (int i = 0; i < splits.splits.size(); i++) {
    //     splitText.setFillColor(layout.fontColor);
    //     splitText.setString(splits.splits[i].name);
    //     splitText.setPosition(0, 50 + (i * 20));
    //     win->draw(splitText);
    // }

    if ((
            sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) ||
            sf::Keyboard::isKeyPressed(sf::Keyboard::RControl)
        ) && (
            sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) ||
            sf::Keyboard::isKeyPressed(sf::Keyboard::RShift)
        )) {
        Application& app = Application::inst();
        std::string state = app.statePath.filename().string();
        std::string instance = app.instance.name;
        instance = instance.empty() ? app.instancePath.filename().string() : instance;
        
        win->setTitle(state + " - " + instance);
    } else {
        win->setTitle(this->style.title);
    }


    win->display();
}


Window::Window() {
    this->style = WindowStyle();
    this->components = std::vector<std::unique_ptr<Component>>();
    this->win = nullptr;
}

Window::~Window() {
    // for (int i = 0; i < this->components.size(); i++) {
    //     delete this->components[i];
    // }
}

void Window::loadJSONEx(std::shared_ptr<tu::json::Node> json) {
    using namespace tu::json;

    auto style = json->getPropertyO("style");
    if (style.has_value()) {
        this->setWindowStyle(
            WindowStyle::fromJSON(style.value())
        );
    }

    auto components = json->getProperty("components")->getOr<Node::array>();
    for (auto componentData : components) {
        this->components.push_back(
            Component::fromJSON(componentData)
        );
    }
}

std::shared_ptr<Window> Window::fromJSON(std::shared_ptr<tu::json::Node> json) {
    std::shared_ptr<Window> result = std::make_shared<Window>();
    result->loadJSONEx(json);
    return result;
}
