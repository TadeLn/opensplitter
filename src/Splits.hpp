#pragma once


#include <string>
#include <vector>

#include "tutils/json/Deserializable.hpp"

#include "Split.hpp"


class Splits : tu::json::Deserializable {

public:
    std::string game;
    std::string category;
    std::vector<Split> splits;

    Splits();

    void loadJSONEx(std::shared_ptr<tu::json::Node> json);
    static Splits fromJSON(std::shared_ptr<tu::json::Node> json);
    static Splits fromFile(std::string filename);

};
