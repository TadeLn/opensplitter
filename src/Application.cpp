#include "Application.hpp"

#define VERSION "0.1.0"


#include <iostream>
#include <chrono>
#include <thread>

#include "tutils/Log.hpp"

#include "util/Util.hpp"
#include "font/FontManager.hpp"
#include "Keybind.hpp"



std::optional<Counter> Application::getCounter(std::string id) {
    auto it = state.counters.find(id);
    if (it != state.counters.end()) {
        return std::optional<Counter>(it->second);
    }
    return std::optional<Counter>();
}

std::optional<Timer> Application::getTimer(std::string id) {
    auto it = state.timers.find(id);
    if (it != state.timers.end()) {
        return std::optional<Timer>(it->second);
    }
    return std::optional<Timer>();
}

std::optional<std::string> Application::getVariable(std::string name) {
    if (name == "game") {
        return std::optional(this->splits.game);
    } else if (name == "category") {
        return std::optional(this->splits.category);
    }
    return std::optional<std::string>();
}



void Application::start(std::vector<std::string>& args) {
    running = true;

    // Load state / instance from file
    if (args.size() < 2) {
        throw tu::Exception("Not enough arguments", EXCTX);
    }
    try {
        loadFile(args[1]);
    } catch (tu::Exception& e) {
        e.print(EXCTX, tu::Exception::LEVEL_ERROR, "Failed to load file from command line argument");
        throw e;
    }

    try {
        FontManager::init();
    } catch (tu::Exception& e) {
        e.print(EXCTX, tu::Exception::LEVEL_ERROR, "Failed to load fonts");
        throw e;
    }

    // Layout has to be loaded after loading fonts
    try {
        loadLayout();
    } catch (tu::Exception& e) {
        e.print(EXCTX, tu::Exception::LEVEL_ERROR, "Failed to load layout");
        throw e;
    }


    float deltaTime;
    std::chrono::system_clock::time_point timestamp = std::chrono::system_clock::now();

    while (isRunning()) {
        std::chrono::system_clock::time_point newTimestamp = std::chrono::system_clock::now();
        deltaTime = (float)(std::chrono::duration_cast<std::chrono::microseconds>(newTimestamp - timestamp).count()) / 1000.f;
        timestamp = newTimestamp;

        loop(deltaTime, timestamp);
        std::this_thread::sleep_for(std::chrono::milliseconds(1));

    }

    close();
}

void Application::stop() {
    running = false;
}

bool Application::isRunning() {
    return running;
}



void Application::reload() {
    tu::Log::log("Reload...");
    saveState();
    loadInstance(instancePath);
    loadLayout();
    // [TODO] loadSplits
}



void Application::init(std::vector<std::string>& args) {
    try {
        Application::__instance = new Application();
        Application::__instance->start(args);
    } catch (tu::Exception& e) {
        e.print(tu::Exception::LEVEL_ERROR, "Initialization failed");
    }
}

Application& Application::inst() {
    return *Application::__instance;
}



void Application::loadFile(std::filesystem::path filename) {
    auto json = tu::json::Node::fromFileEx(filename.string());
    std::string fileType = json->getPropertyEx("type")->getEx<std::string>();

    if (fileType == "instance") {
        tu::Log::log("Loading instance from args: ", filename, " -> ", json);
        instancePath = filename;
        loadInstance(json);
        statePath = std::filesystem::path("states") / tu::String::str("state_", tu::Date::now().toSnakeCaseString(), ".tsplit");

    } else if (fileType == "state") {
        tu::Log::log("Loading state from args: ", filename,  " -> ", json);
        statePath = filename;
        loadState(json);
        loadInstance(statePath.parent_path() / state.instance);
    } else {
        throw tu::Exception("Invalid file format", EXCTX);
    }
}


void Application::loadInstance(std::filesystem::path filename) {
    tu::Log::log("Loading instance from file: ", filename);
    instancePath = filename;
    return loadInstance(tu::json::Node::fromFileEx(filename.string()));
}

void Application::loadInstance(std::shared_ptr<tu::json::Node> json) {
    this->instance = Instance::fromJSON(json);
}


void Application::loadState(std::filesystem::path filename) {
    tu::Log::log("Loading state from file: ", filename);
    statePath = filename;
    return loadState(tu::json::Node::fromFileEx(filename.string()));
}

void Application::loadState(std::shared_ptr<tu::json::Node> json) {
    this->state = State::fromJSON(json);
}



void Application::loadLayout() {
    // The state should be saved before loading a new layout, since it might crash or something

    tu::Log::log("Loading layout");
    {
        auto windows = layout.getWindows();
        for (auto window : windows) {
            window.second->close();
        }
    }

    layout = Layout::fromFile((instancePath.parent_path() / instance.layout).string());

    {
        auto windows = layout.getWindows();
        for (auto window : windows) {
            window.second->open();
        }
    }
}

void Application::loadSplits() {
    tu::Log::log("Loading splits");
    splits = Splits::fromFile((instancePath.parent_path() / instance.splits).string());
}




void Application::saveState() {
    saveState(statePath);
}

void Application::saveState(std::filesystem::path filename) {
    // If the state doesn't have an instance path, create it
    if (state.instance.empty()) {
        state.instance = std::filesystem::absolute(instancePath).string();
    }

    std::string str = filename.string();
    auto json = state.toJSON();

    tu::Log::log("Saving state to file: ", json, " -> ", str);

    json->toFile(str);
}




void Application::loop(float deltaTime, std::chrono::system_clock::time_point currentTime) {
    auto windows = layout.getWindows();

    sf::Event e;
    for (auto window : windows) {
        std::shared_ptr<sf::RenderWindow> win = window.second->win;

        while (win->pollEvent(e)) {

            switch (e.type) {

                case sf::Event::Closed:
                    win->close();
                    return;

                case sf::Event::Resized:
                    win->setView(sf::View(sf::FloatRect(0, 0, e.size.width, e.size.height)));
                    break;

                case sf::Event::KeyPressed:
                    if (!instance.settings.globalKeys) {
                        handleKeyEvent(e.key);
                    }
                    break;
                
                default:
                    break;
            }
        }
    }

    if (instance.settings.globalKeys) {
        checkGlobalKeys();
    }

    windows = layout.getWindows();
    for (auto window : windows) {
        window.second->draw();
        if (!window.second->win->isOpen()) {
            stop();
        }
    }
}



void Application::handleKeyEvent(sf::Event::KeyEvent e) {
    if (e.control && e.shift) {
        tu::Log::info("Key Code: ", e.code);
    }

    if (instance.settings.keybinds.reload.checkEvent(e)) {
        reload();
    }

    for (auto& counter : state.counters) {
        counter.second.handleKeyEvent(e);
    }

    for (auto& timer : state.timers) {
        timer.second.handleKeyEvent(e);
    }
}



void Application::checkGlobalKeys() {
    auto setKeyState = [&](int index, bool value) {
        if (index < 0) {
            return;
        }
        while (currentKeyboardState.size() <= index) {
            currentKeyboardState.push_back(false);
        }
        currentKeyboardState[index] = value;
    };

    for (int i = 0; i < sf::Keyboard::KeyCount; i++) {
        setKeyState(i, sf::Keyboard::isKeyPressed((sf::Keyboard::Key)i));
    }

    for (int i = 0; i < lastKeyboardState.size(); i++) {
        if (currentKeyboardState[i] != lastKeyboardState[i]) {
            if (currentKeyboardState[i]) {
                sf::Event::KeyEvent keyEvent = {
                    .code =    (sf::Keyboard::Key)i,
                    .alt =     sf::Keyboard::isKeyPressed(sf::Keyboard::LAlt)     || sf::Keyboard::isKeyPressed(sf::Keyboard::RAlt),
                    .control = sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) || sf::Keyboard::isKeyPressed(sf::Keyboard::RControl),
                    .shift =   sf::Keyboard::isKeyPressed(sf::Keyboard::LShift)   || sf::Keyboard::isKeyPressed(sf::Keyboard::RShift),
                    .system =  sf::Keyboard::isKeyPressed(sf::Keyboard::LSystem)  || sf::Keyboard::isKeyPressed(sf::Keyboard::RSystem)
                };
                handleKeyEvent(keyEvent);
            }
        }
    }

    lastKeyboardState = currentKeyboardState;
}



void Application::close() {
    saveState();

    auto windows = layout.getWindows();
    for (auto window : windows) {
        window.second->close();
    }
}



Application* Application::__instance = nullptr;
