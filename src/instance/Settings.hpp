#pragma once


#include "tutils/json/Deserializable.hpp"

#include "../Keybind.hpp"



class Settings : public tu::json::Deserializable {

public:
    bool globalKeys;

    struct {
        Keybind reload;
    } keybinds;

    void loadJSONEx(std::shared_ptr<tu::json::Node> json);
    static Settings fromJSON(std::shared_ptr<tu::json::Node> json);

};