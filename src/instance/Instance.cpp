#include "Instance.hpp"

#include "tutils/json/Object.hpp"
#include "tutils/json/String.hpp"

#include <iostream>



void Instance::loadJSONEx(std::shared_ptr<tu::json::Node> json) {
    using namespace tu::json;

    if (json->getProperty("type")->getOr<std::string>() != "instance") throw tu::Exception("Incorrect format", EXCTX);
    
    json->getProperty("name")->load(name);
    json->getProperty("layout")->load(layout);
    json->getProperty("splits")->load(splits);
    settings = Settings::fromJSON(json->getProperty("settings"));
}



Instance Instance::fromJSON(std::shared_ptr<tu::json::Node> json) {
    Instance result;
    result.loadJSONEx(json);
    return result;
}

