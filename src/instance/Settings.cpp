#include "Settings.hpp"

#include <iostream>

#include "../util/Util.hpp"



void Settings::loadJSONEx(std::shared_ptr<tu::json::Node> json) {
    globalKeys = json->getProperty("globalKeys")->getOr(false);

    auto k = json->getProperty("keybinds");
    keybinds.reload = Keybind::fromJSON(k->getProperty("reload"));
}

Settings Settings::fromJSON(std::shared_ptr<tu::json::Node> json) {
    Settings result;
    result.loadJSONEx(json);
    return result;
}
