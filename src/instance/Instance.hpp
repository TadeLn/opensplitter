#pragma once


#include <map>
#include <string>
#include <filesystem>

#include "tutils/json/Deserializable.hpp"
#include "tutils/json/Serializable.hpp"

#include "Settings.hpp"



class Instance : public tu::json::Deserializable {

public:
    std::string name;
    std::string layout;
    std::string splits;
    Settings settings;

    void loadJSONEx(std::shared_ptr<tu::json::Node> json);
    static Instance fromJSON(std::shared_ptr<tu::json::Node> json);

};
