#include "Util.hpp"


#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <utility>
#include <string>
#include <regex>
#include "font/FontManager.hpp"
#include "Application.hpp"



std::string Util::toHex(std::int64_t number) {
    std::stringstream ss;
    ss << std::hex << number;
    return ss.str();
}

std::string Util::msToString(unsigned int ms, int decimalPlaces) {
    std::string result;

    if (ms < 0) {
        ms = -ms;
        result += "-";
    }

    unsigned int milliseconds = ms % 1000;
    unsigned int seconds      = ms / 1000 % 60;
    unsigned int minutes      = ms / 1000 / 60 % 60;
    unsigned int hours        = ms / 1000 / 60 / 60;

    if (hours > 0) {
        result += std::to_string(hours);
        result += ":";
    }
    if (hours > 0 || minutes > 0) {
        if (ms > 1000 * 60 * 60) {
            result += Util::lpadding(std::to_string(minutes), 2, '0');
        } else {
            result += std::to_string(minutes);
        }
        result += ":";
    }
    if (ms > 1000 * 60) {
        result += Util::lpadding(std::to_string(seconds), 2, '0');
    } else {
        result += std::to_string(seconds);
    }
    if (decimalPlaces > 0) {
        if (decimalPlaces > 3) {
            decimalPlaces = 3;
        }
        result += ".";
        result += Util::lpadding(std::to_string(milliseconds), 3, '0').substr(0, decimalPlaces);
    }
    
    return result;
}

std::string Util::lpadding(std::string src, unsigned int length, char character) {
    if (src.length() < length) {
        std::string padding;
        for (int i = 0; i < length - src.length(); i++) {
            padding.push_back(character);
        }
        return padding + src;
    }
    return src;
}

std::string Util::formatString(std::string string, std::vector<std::string> args) {
    for (int i = 0; i < args.size(); i++) {
        string = std::regex_replace(string, std::regex(
            std::string("\\{") + std::to_string(i) + std::string("\\}")
        ), args[i]);
    }
    return string;
}



std::optional<sf::Color> Util::getColor(std::shared_ptr<tu::json::Node> json) {
    using namespace tu::json;

    std::optional<sf::Color> result;

    if (json->getType() == Node::STRING) {
        std::string str = json->getOr<std::string>();
        std::uint64_t value = -1;
        try {
            value = std::stoll(str, 0, 16);
            // If the color is RGB and not RGBA, set alpha to 0xff (100%)
            if (str.length() < 8) {
                value <<= 8;
                value |= 0xff;
            }       
        } catch (std::exception& e) {
        }

        if (value >= 0 && value <= 0xffffffff) {
            result = std::optional(sf::Color(value));
        }

    } else if (json->getType() == Node::UINT) {
        std::uint64_t value = json->getOr<std::uint64_t>();
        if (value >= 0 && value <= 0xffffffff) {
            result = std::optional(sf::Color(value));
        }
    }

    return result;
}
std::optional<sf::Vector2f> Util::getVector2f(std::shared_ptr<tu::json::Node> json) {
    using namespace tu::json;

    if (json->getType() == Node::ARRAY) {
        auto elem0 = json->getElementO(0);
        auto elem1 = json->getElementO(1);
        if (elem0.has_value() && elem1.has_value()) {
            auto x = elem0.value()->getFloat();
            auto y = elem1.value()->getFloat();
            if (x.has_value() && y.has_value()) {
                return std::optional(sf::Vector2f(
                    x.value(), y.value()
                ));
            }
        }
    } else if (json->getType() == Node::OBJECT) {
        auto propX = json->getPropertyO("x");
        auto propY = json->getPropertyO("y");
        if (propX.has_value() && propY.has_value()) {
            auto x = propX.value()->getFloat();
            auto y = propY.value()->getFloat();
            if (x.has_value() && y.has_value()) {
                return std::optional(sf::Vector2f(
                    x.value(), y.value()
                ));
            }
        }
    }

    return std::optional<sf::Vector2f>();
}
