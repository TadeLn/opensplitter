#pragma once


#include <string>

#include <SFML/Graphics.hpp>

#include "tutils/json/Node.hpp"

#include "Application.hpp"
#include "font/FontManager.hpp"


namespace Util {


// Load a value from optional into reference if it exists
// Returns true on success
template<typename T>
bool load(std::optional<T> optional, T& ref) {
    if (optional.has_value()) {
        ref = optional.value();
        return true;
    }
    return false;
}

std::string toHex(std::int64_t number);
std::string msToString(unsigned int ms, int decimalPlaces);
std::string lpadding(std::string src, unsigned int length, char character);
std::string formatString(std::string string, std::vector<std::string> args);

std::optional<sf::Color> getColor(std::shared_ptr<tu::json::Node> json);
std::optional<sf::Vector2f> getVector2f(std::shared_ptr<tu::json::Node> json);


};