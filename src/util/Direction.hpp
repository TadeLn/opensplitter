#pragma once



enum Direction {

    UNDEFINED = -1,

    NEG_X = 0,
    POS_X,
    NEG_Y,
    POS_Y,
    X,
    Y,

    WEST = NEG_X,
    EAST = POS_X,
    NORTH = NEG_Y,
    SOUTH = POS_Y,
    HORIZONTAL = X,
    VERTICAL = Y

};
