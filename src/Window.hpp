#pragma once


#include <vector>

#include "tutils/json/Deserializable.hpp"

#include "style/WindowStyle.hpp"
#include "component/Component.hpp"



class Window : tu::json::Deserializable {


public:
    std::vector<std::unique_ptr<Component>> components;
    std::shared_ptr<sf::RenderWindow> win;

    WindowStyle getWindowStyle();
    void setWindowStyle(WindowStyle style);

    void open();
    void close();
    void draw();

    Window();
    ~Window();
    
    void loadJSONEx(std::shared_ptr<tu::json::Node> json);
    static std::shared_ptr<Window> fromJSON(std::shared_ptr<tu::json::Node> json);

protected:
    void recreateWindow();

private:
    WindowStyle style;


};
