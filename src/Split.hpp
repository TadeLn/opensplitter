#pragma once


#include <string>

#include "tutils/json/Deserializable.hpp"



class Split : public tu::json::Deserializable {

public:
    std::string name;
    std::string desc;

    Split();

    void loadJSONEx(std::shared_ptr<tu::json::Node> json);
    static Split fromJSON(std::shared_ptr<tu::json::Node> json);


};
