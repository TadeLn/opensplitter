#pragma once


#include <map>
#include <string>

#include <SFML/Graphics.hpp>

#include "FontGroup.hpp"



class FontManager {


public:
    static std::shared_ptr<sf::Font> getDefault();

    static std::shared_ptr<sf::Font> get(std::string key, std::string variant);
    static std::string getName(std::string key);
    static const std::optional<FontGroup> getEntry(std::string key);
    
    static void add(std::string key, FontGroup fontGroup);

    static void init();
    static void fromFile(std::string filename);

private:
    static std::map<std::string, FontGroup> fonts;
    static std::shared_ptr<sf::Font> defaultFont;


};
