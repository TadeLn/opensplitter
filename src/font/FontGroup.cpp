#include "FontGroup.hpp"

#include "tutils/Log.hpp"

#include <iostream>



std::string FontGroup::getName() const {
    return this->name;
}



std::shared_ptr<sf::Font> FontGroup::getVariant(std::string variant) {
    auto it = variants.find(variant);
    if (it != variants.end()) {
        return it->second;
    } else {
        return nullptr;
    }
}

const std::map<std::string, std::shared_ptr<sf::Font>>& FontGroup::getVariants() const {
    return this->variants;
}

std::string FontGroup::getDefaultVariant() const {
    if (variants.empty()) {
        return "";
    }
    return this->variants.begin()->first;
}



void FontGroup::loadJSONEx(std::shared_ptr<tu::json::Node> json) {
    using namespace tu::json;

    json->getProperty("name")->load(name);

    auto variants = json->getProperty("variants")->getOr<Node::dictionary>();
    for (auto pair : variants) {
        auto path = std::filesystem::path("res/font");
        if (pair.second->getType() == Node::STRING) {
            path /= pair.second->getOr<std::string>("abc");
        } else {
            path /= pair.second->getProperty("path")->getOr<std::string>();
        }

        if (this->variants.find(pair.first) == this->variants.end()) {
            // tu::Log::log("  Adding font variant '", pair.first, "'");
            std::shared_ptr<sf::Font> font = std::make_shared<sf::Font>();
            if (font->loadFromFile(path.string())) {
                this->variants[pair.first] = font;
            } else {
                tu::Log::error("    An error occurred while loading variant '", pair.first, "'");
            }
        }
    }
}

FontGroup FontGroup::fromJSON(std::shared_ptr<tu::json::Node> json) {
    FontGroup result;
    result.loadJSONEx(json);
    return result;
}

