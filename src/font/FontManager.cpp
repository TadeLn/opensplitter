#include "FontManager.hpp"


#include <iostream>

#include "tutils/Log.hpp"
#include "tutils/json/Node.hpp"
#include "tutils/json/Array.hpp"



std::shared_ptr<sf::Font> FontManager::getDefault() {
    return defaultFont;
}

std::shared_ptr<sf::Font> FontManager::get(std::string key, std::string variant) {
    std::shared_ptr<sf::Font> font;

    auto entry = getEntry(key);
    if (entry.has_value()) {
        font = entry.value().getVariant(variant);
    }

    if (font == nullptr) {
        return getDefault();
    }
    return font;
}

std::string FontManager::getName(std::string key) {
    auto entry = getEntry(key);
    if (entry.has_value()) {
        return entry.value().getName();
    }
    return "";
}

const std::optional<FontGroup> FontManager::getEntry(std::string key) {
    auto it = fonts.find(key);
    if (it != fonts.end()) {
        return std::optional<FontGroup>(it->second);
    }
    return std::optional<FontGroup>();
}


void FontManager::add(std::string key, FontGroup fontGroup) {
    tu::Log::log("Added font '", fontGroup.getName(), "' (", key, ")");
    if (!defaultFont) {
        defaultFont = fontGroup.getVariant(fontGroup.getDefaultVariant());
        tu::Log::log("Set default to: ", key, "/", fontGroup.getDefaultVariant(), " ptr: ", defaultFont);
    }
    
    fonts.insert_or_assign(key, fontGroup);
}



void FontManager::init() {
    fromFile("res/font/fonts.json");
}

void FontManager::fromFile(std::string filename) {
    defaultFont = std::shared_ptr<sf::Font>();
    // defaultFont->loadFromFile("res/font/open_sans/static/OpenSans/OpenSans-Regular.ttf");
    // defaultFont->loadFromFile("res/font/annie_use_your_telescope/AnnieUseYourTelescope-Regular.ttf");
    fonts.clear();

    using namespace tu::json;
    auto json = Node::fromFileEx(filename);

    std::map<std::string, std::string> variantNames;

    auto variants = json->getProperty("variants")->getOr<Node::dictionary>();
    for (auto pair : variants) {
        variantNames[pair.first] = pair.second->getOr<std::string>();
    }
    
    auto fonts = json->getProperty("fonts")->getOr<Node::dictionary>();
    for (auto pair : fonts) {
        add(pair.first, FontGroup::fromJSON(pair.second));
    }

}



std::map<std::string, FontGroup> FontManager::fonts;
std::shared_ptr<sf::Font> FontManager::defaultFont;
