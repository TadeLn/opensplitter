#pragma once


#include <map>
#include <string>

#include <SFML/Graphics.hpp>

#include "tutils/json/Deserializable.hpp"



class FontGroup : tu::json::Deserializable {

public:
    std::string getName() const;

    std::shared_ptr<sf::Font> getVariant(std::string variant);
    const std::map<std::string, std::shared_ptr<sf::Font>>& getVariants() const;
    std::string getDefaultVariant() const;

    void loadJSONEx(std::shared_ptr<tu::json::Node> json);
    static FontGroup fromJSON(std::shared_ptr<tu::json::Node> json);

private:
    std::string name;
    std::map<std::string, std::shared_ptr<sf::Font>> variants;

};
