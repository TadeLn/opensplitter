#pragma once


#include <memory>
#include <optional>

#include <SFML/Window.hpp>

#include "tutils/json/Node.hpp"



class Keybind {

public:
    int code = -1;
    std::optional<bool> control;
    std::optional<bool> alt;
    std::optional<bool> shift;
    std::optional<bool> system;

    bool checkEvent(sf::Event::KeyEvent e);

    Keybind(int code, std::optional<bool> control, std::optional<bool> alt, std::optional<bool> shift, std::optional<bool> system);
    Keybind();

    void loadJSONEx(std::shared_ptr<tu::json::Node> json);
    static Keybind fromJSON(std::shared_ptr<tu::json::Node> json);

};
